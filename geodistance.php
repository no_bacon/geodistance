<?php

namespace GeoDistance;

use ArrayIterator;
use FilterIterator;
use Iterator;

class GeoDistanceApp
{

    public static function main(Location $ourLocation, array $clientLocations)
    {
        $filter = new DistanceFilterIterator(
            new ArrayIterator($clientLocations),
            new HaversineDistance(),
            $ourLocation
        );

        header("Content-type: text/plain; charset=utf-8");

        foreach ($filter as $client) {
            echo $client->getName() . "\n";
        }
    }
}

class LatLon
{
    private $lon;
    private $lat;

    /**
     * LongLat constructor.
     * @param $lon
     * @param $lat
     */
    public function __construct($lon, $lat)
    {
        $this->lon = $lon;
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }
}

interface DistanceFormula
{
    public function calculateDistance(LatLon $a, LatLon $b);
}

class HaversineDistance implements DistanceFormula
{

    const MEAN_RADIUS_OF_EARTH_IN_MILES = 3959;

    public function calculateDistance(LatLon $a, LatLon $b)
    {
        $dlat = deg2rad($b->getLat() - $a->getLat());
        $dlon = deg2rad($b->getLon() - $a->getLon());

        $a = sin($dlat / 2) * sin($dlat / 2) +
            cos(deg2rad($a->getLat())) * cos(deg2rad($b->getLat())) *
            sin($dlon / 2) * sin($dlon);

        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        return self::MEAN_RADIUS_OF_EARTH_IN_MILES * $c;
    }

}

class DistanceFilterIterator extends FilterIterator
{

    private $location;
    private $formula;

    public function __construct(Iterator $iterator, DistanceFormula $formula, Location $location)
    {
        parent::__construct($iterator);
        $this->location = $location;
        $this->formula = $formula;
    }

    public function accept()
    {
        $distance = $this->formula->calculateDistance(
            $this->location->getLatLon(),
            $this->current()->getLatLon()
        );

        return $distance < 50;
    }
}

class Location
{
    private $latLon;
    private $name;

    /**
     * Location constructor.
     * @param LatLon $latLon
     * @param $name
     */
    public function __construct(LatLon $latLon, $name)
    {
        $this->latLon = $latLon;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return LatLon
     */
    public function getLatLon()
    {
        return $this->latLon;
    }
}

// -----------------------------------------------------------------------

$locs = '[
    {"name":"Mike","lat":52.914335,"lon":-0.649357},
    {"name":"Paul","lat":53.065463,"lon":-0.802284},
    {"name":"Ian","lat":53.305097,"lon":-0.932078},
    {"name":"Hyde Park Trading Estate","lat":52.998172,"lon":-2.180799}
]';

$locations = [];

foreach (json_decode($locs) as $location) {
    $locations[] = new Location(
        new LatLon($location->lat, $location->lon),
        $location->name
    );
}

GeoDistanceApp::main(new Location(new LatLon(52.898847, -0.646619), "Payplan"), $locations);